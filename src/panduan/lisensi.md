# Lisensi

> Penggunaan lisensi dibawah ini hanya sebagai contoh.

Website ini dan [kode sumbernya](https://gitlab.com/langitketujuh.id/inkscape) berada dibawah lisensi [Atribusi-NonKomersial 4.0 Internasional (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/).

**Anda diperbolehkan untuk:**

- **Berbagi** — menyalin dan menyebarluaskan kembali materi ini dalam bentuk atau format apapun;
- **Adaptasi** — menggubah, mengubah, dan membuat turunan dari materi ini

Pemberi lisensi tidak dapat mencabut ketentuan di atas sepanjang Anda mematuhi ketentuan lisensi ini.

**Berdasarkan ketentuan berikut:**

- **Atribusi** — Anda harus mencantumkan [nama yang sesuai](# "Apabila tersedia, Anda harus mencantumkan nama pencipta dan pihak terkait, pemberitahuan hak cipta, pemberitahuan lisensi, sangkalan, dan tautan terhadap materi yang digunakan. Lisensi CC sebelum Versi 4.0 juga mengharuskan Anda untu mencantumkan judul materi yang digunakan apabila tersedia, dan mungkin memiliki beberapa persyaratan yang berbeda."), mencantumkan tautan terhadap lisensi, dan [menyatakan bahwa telah ada perubahan yang dilakukan](# "Pada versi 4.0 ini, Anda harus menyatakan Apabila Anda mengubah materi yang digunakan dan mengindikasikan perubahan sebelumnya. Pada versi lisensi 3.0 dan sebelumnya, indikasi perubahan diperlukan apabila Anda membuat ciptaan turunan."). Anda dapat melakukan hal ini dengan cara yang sesuai, namun tidak mengisyaratkan bahwa pemberi lisensi mendukung Anda atau penggunaan Anda.

- **NonKomersial** — Anda tidak dapat menggunakan materi ini untuk [kepentingan komersial](# "Penggunaan komersial adalah penggunaan untuk memperoleh keuntungan komersial atau kompensasi dalam bentuk uang.").

Tidak ada pembatasan tambahan — Anda tidak dapat menggunakan ketentuan hukum atau sarana kontrol teknologi yang secara hukum membatasi orang lain untuk melakukan hal-hal yang diizinkan lisensi ini.
