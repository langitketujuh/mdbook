# Daftar isi

- [Beranda](./index.md)
- [Kata Pengantar](./kata-pengantar.md)
- [Panduan](./panduan/index.md)
  - [Fitur Utama](./panduan/fitur-utama.md)
  - [Lisensi Konten](./panduan/lisensi.md)

---
- [Tentang Penulis](./penulis.md)
